import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css' //check to see if this should be after other style sheets
import './App.css'
import { Switch, Route, Redirect } from 'react-router-dom'
import Navigation from './navigation/Navigation'

import About from './static/About'
import LoginForm from './authentication/LoginForm'
import SignUpForm from './authentication/SignUpForm'
import ObjectiveList from './objectives/ObjectiveList'
import ObjectiveEditForm from './objectives/ObjectiveEditForm'
import KeyResultEditForm from './keyResults/KeyResultEditForm'
import Welcome from './authentication/Welcome'
import { useUser } from './hooks/UseUser'

function App() {
  const { user } = useUser()
  return (
    <div className='App'>
      <Navigation />
      <Switch>
        <Route exact path='/'>
          {user.name ? <Redirect to='objectiveList' /> : <Welcome />}
        </Route>
        <Route exact path='/about' component={About} />
        <Route exact path='/signUp'>
          {user.name ? <Redirect to='objectiveList' /> : <SignUpForm />}
        </Route>
        <Route exact path='/login'>
          {user.name ? <Redirect to='objectiveList' /> : <LoginForm />}
        </Route>
        <Route path='/objectiveList'>
          {!user.name ? <Redirect to='/' /> : <ObjectiveList />}
        </Route>
        <Route path='/objectiveEdit'>
          <ObjectiveEditForm />
        </Route>
        <Route path='/keyResultEdit'>
          <KeyResultEditForm />
        </Route>
      </Switch>
    </div>
  )
}

export default App

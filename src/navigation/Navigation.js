import React from 'react'
import { Navbar, Nav } from 'react-bootstrap'
import { NavLink } from 'react-router-dom'
import { useUser } from '../hooks/UseUser'
import LoginForm from '../authentication/LoginForm'
import SignUpForm from '../authentication/SignUpForm'

const Navigation = props => {
  const { setAccessToken } = useUser()
  const { user } = useUser()

  function logOut() {
    setAccessToken(null)
    //Add logout API call??
  }
  return (
    <Navbar
      collapseOnSelect
      expand='lg'
      bg='dark'
      variant='dark'
      className='headerBackground'
    >
      <Navbar.Brand as={NavLink} exact to='/'>
        OpenOKR
      </Navbar.Brand>
      <Navbar.Toggle aria-controls='responsive-navbar-nav' />
      <Navbar.Collapse id="responsive-navbar-nav">
        <Nav className="mr-auto">
        {/* <Nav.Link as={NavLink} exact to="/objectiveList">Objectives</Nav.Link> */}
          <Nav.Link className='navLinks'>Start Here</Nav.Link>
          <Nav.Link as={NavLink} exact to='/about' className='navLinks'>
            About
          </Nav.Link>
          <Nav.Link className='navLinks'>Contributing</Nav.Link>
          <Nav.Link className='navLinks'>Contact</Nav.Link>
          {/* <Nav.Link className='ml-auto navLinks'>Login</Nav.Link>? */}
          {/* <Nav.Link className='navLinks'>Signup</Nav.Link> */}
          {user.name && <Nav.Link onClick={logOut}>Logout</Nav.Link>}
        </Nav>
        <Nav>
          {!user.name && <LoginForm  />}
          {!user.name && <SignUpForm />}
        </Nav>
      </Navbar.Collapse>
    </Navbar>
  )
}
export default Navigation

import React from 'react';
import { shallow } from 'enzyme';
import Navigation from './Navigation'

describe('Navigation bar', () => {
    const wrapper = shallow(<Navigation />);

    it('should have a Nav.Link component', () => {
        expect(wrapper.find('Nav.Link')).toHaveLength(2);

        expect(wrapper.find('Nav.Link').text()).toEqual('Log in' || 'Log out');
    });
});
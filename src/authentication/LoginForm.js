import React, { useState } from 'react'
import { Form, Container, Alert } from 'react-bootstrap'
import axios from 'axios'
import { useUser } from '../hooks/UseUser'
import { Modal, Button, Input } from 'react-rainbow-components';
import { buttonStyle, addButtonStyle } from '../inlinestyles'
import { API_LOGIN } from '../api/APIUtils'
import { useHistory} from 'react-router-dom';

const styles = {
  input: {
    marginTop: 24,
  },
  inputColumn: {
    width: '48%',
    marginTop: 24,
  }
};

const LoginForm = () => {
  const [loginData, setLoginData] = useState({ email: '', password: '' })
  const [isOpen, setOpenStatus] = useState(false);
  const { setAccessToken } = useUser()
  const [show, setShow] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  let history = useHistory();

  const handleChange = event => {
    setLoginData({ ...loginData, [event.target.name]: event.target.value })
  }

  function login(e) {
    e.preventDefault()
    axios({
      method: 'post',
      url: `${API_LOGIN}`,
      headers: {
        'Content-Type': 'application/json'
      },
      data: loginData
    })
      .then(res => {
        setAccessToken(res.data.auth_token)
        setOpenStatus(false);
        setLoginData({ email: '', password: '' })
        history.push("/objectiveList")

      })
      .catch(err => {
        setShow(true);
        setErrorMessage(err.response.data.message)
      })
  }

  return (
    <Container className="m-1em">
      <Button
        id="button-login"
        style={buttonStyle}
        label="Login"
        onClick={() => setOpenStatus(true)}
      />
      <Modal
        id="modal-login"
        title="Login"
        isOpen={isOpen}
        onRequestClose={() => setOpenStatus(false)}
        >
        {show
          &&
          <Alert variant="warning" onClose={() => setShow(false)} dismissible>
            <Alert.Heading>Hmmm.... You got an error!</Alert.Heading>
            <p>{errorMessage}</p>
          </Alert>
        }
        <Form onSubmit={login}>
          <Form.Group>
            <Input
              id='email'
              style={styles.input}
              component={Input}
              type='email'
              name='email'
              label="Email"
              placeholder='Email'
              value={loginData.email}
              onChange={handleChange}
              required
              />
          </Form.Group>
          <Form.Group>
            <Input
              id='password'
              style={styles.input}
              component={Input}
              type='password'
              name='password'
              label="Password"
              onChange={handleChange}
              placeholder='Password'
              required
              />
          </Form.Group>

          <Form.Group>
            <Button
              type="submit"
              label='Login'
              style={addButtonStyle}
              />
          </Form.Group>
        </Form>
      </Modal>
    </Container>
  )
}

export default LoginForm

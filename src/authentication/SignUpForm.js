import React, { useState } from 'react'
import {Form, Container, Alert} from 'react-bootstrap'
import axios from 'axios'
import { useUser } from '../hooks/UseUser'
import { Modal, Button, Input } from 'react-rainbow-components'
import { buttonStyle, addButtonStyle } from '../inlinestyles'
import { API_SIGNUP } from '../api/APIUtils'
import { useHistory } from 'react-router-dom';

const styles = {
  input: {
    marginTop: 24,
  },
  inputColumn: {
    width: '48%',
    marginTop: 24,
  }
};

const initialState = { name: '', email: '', password: '' }

const SignUpForm = () => {
  const [signUpData, setSignUpData] = useState(initialState)
  const [isOpen, setOpenStatus] = useState(false);
  const { setAccessToken } = useUser()
  const [show, setShow] = useState(false);
  const [errorMessage, setErrorMessage] = useState("");
  let history = useHistory();

  const handleChange = event => {
    setSignUpData({ ...signUpData, [event.target.name]: event.target.value })
  }

  const signUp = e => {
    e.preventDefault()
    axios({
      method: 'post',
      url: `${API_SIGNUP}`,
      headers: {
        'Content-Type': 'application/json'
      },
      data: signUpData
    })
    .then(res => {
      setAccessToken(res.data.auth_token);
      setOpenStatus(false)
      history.push("/objectiveList")
    })
    .catch(err => {
      setShow(true);
      setErrorMessage(err.response.data.message)
      })

  }

  return (
    <Container className="m-1em">
      <Button 
        id="button-signup"
        style={buttonStyle}
        label="Signup"
        onClick={() => setOpenStatus(true)}
        />
      <Modal
        id="modal-signup"
        title="Signup"
        isOpen={isOpen}
        onRequestClose={() => setOpenStatus(false)}
        >
        {show 
        &&
          <Alert variant="warning" onClose={() => setShow(false)} dismissible>
            <Alert.Heading>Hmmm.... You got an error!</Alert.Heading>
            <p>{errorMessage}</p>
            </Alert>
        }
        <Form onSubmit={signUp}>
          <Form.Group controlId="name">
            <Input
              id='name'
              style={styles.input}
              component={Input}
              type='text'
              name='name'
              label="Full Name"
              placeholder='Full Name'
              value={signUpData.name}
              onChange={handleChange}
              required
              />
          </Form.Group>
          <Form.Group controlId="email">
            <Input
              id='email'
              style={styles.input}
              component={Input}
              type='email'
              name='email'
              label="Email"
              placeholder='Email'
              value={signUpData.email}
              onChange={handleChange}
              required
              />
          </Form.Group>
          <Form.Group>
            <Input
              id='password'
              style={styles.input}
              component={Input}
              type='password'
              name='password'
              label="Password"
              onChange={handleChange}
              placeholder='Password'
              bottomHelpText="Minimum 8 characters with at least one uppercase, one lowercase, one digit, and one special character"
              required
              />
          </Form.Group>
          <Form.Group>
            <Button
              type="submit"
              label="Sign Up"
              style={addButtonStyle}
              />
          </Form.Group>
        </Form>
      </Modal>
    </Container>
  )
}

export default SignUpForm

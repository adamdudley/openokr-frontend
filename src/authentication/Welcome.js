import React from 'react'
import { Container } from 'react-bootstrap'
import SignUpForm from './SignUpForm'

export default function Welcome() {
  return (
    <Container className='mainContainer' text='center'>
      <h1 className='mainHeading'>Work smarter with OpenOKR</h1>
      <br/>
      <SignUpForm />
    </Container>
  )
}

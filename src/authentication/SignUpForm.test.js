// Adapted from https://dev.to/richardigbiriki/testing-your-first-react-component-with-jest-and-enzyme-p38

import React from 'react';
import { shallow } from 'enzyme';
import SignUpForm from './SignUpForm'

describe('SignUp form', () => {
    const wrapper = shallow(<SignUpForm />);

    it('should have a btn component', () => {
        //There should be only one button
        expect(wrapper.find('Button')).toHaveLength(1);

        //Button should be of type button
        expect(wrapper.find('Button')
            .type().defaultProps.type)
            .toEqual('button');

        //Button should have matching text
        expect(wrapper.find('Button').text()).toEqual('Sign Up');
    });

    it('should have input for email and password', () => {
        //Email and password input field should be present
        expect(wrapper.find('#email')).toHaveLength(1);
        expect(wrapper.find('#password')).toHaveLength(1);
        expect(wrapper.find('#name')).toHaveLength(1);
    });
})
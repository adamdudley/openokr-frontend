export const buttonStyle = {
  backgroundColor: '#3a443b',
  color: '#fff',
  fontFamily: 'Montserrat'
}

export const mainButtonStyle = {}

export const editButtonStyle = {
  backgroundColor: '#3a443b',
  color: '#d6d2b0',
  fontFamily: 'Titillium Web'
}
export const addButtonStyle = {
  backgroundColor: '#2d795b',
  color: '#d6d2b0',
  fontFamily: 'Titillium Web',
  marginTop: '2em',
  display: 'block',
  width: '100%',
  textAlign: 'center'
}
export const cancelButtonStyle = {
  backgroundColor: '#d5b02d',
  color: '#070707',
  fontFamily: 'Titillium Web',
  marginTop: '2em',
  display: 'block',
  width: '100%',
  textAlign: 'center'
}
export const deleteButtonStyle = {
  backgroundColor: '#d5b02d',
  color: '#070707',
  fontFamily: 'Titillium Web'
  // marginRight: "1em",
  // width: "10em"
}

export const alertStyle = {
  backgroundColor: '#D5B02D',
  color: '#fff',
  display: 'flex',
  flexDirection: 'column',
  alignItems: 'center',
  justifyContent: 'center'
}

export const keyResultStyle = {
  backgroundColor: '#d6d2b0'
}

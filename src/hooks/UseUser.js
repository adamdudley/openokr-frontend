//Used this resource to make this hook https://codious.io/user-management-with-react-hooks/
import React, { createContext, useContext, useEffect, useState } from 'react';

function getCurrentUser(accessToken) {
    //This function could be used to fetch the users name.
    if(accessToken) {
        return {
            name: 'Awesome OKR User',
        };
    }
}

const initialState = {
    user: {},
    accessToken: undefined,
};

const UserContext = createContext(initialState);

export function UserProvider({children}) {
    const [accessToken, setAccessToken] = useState(localStorage.getItem('token')); //the example has .getItem('access_token'), but this app uses 'token' as the key
    const [user, setUser] = useState({});

    
    useEffect(() => {
        function handleAccessTokenChange() {
            if (!user.name && accessToken){
                localStorage.setItem('token', accessToken); // unclear about this link
                const user = getCurrentUser(accessToken);
                setUser(user);
            } else if (!accessToken) {
                //Log out
                localStorage.removeItem('token');
                setUser({});
            }
        }
        handleAccessTokenChange();
    }, [accessToken, user.name]);

    return (
        <UserContext.Provider value={{ user, accessToken, setAccessToken }}>
            {children}
        </UserContext.Provider>
    );
}

export const useUser = () => useContext(UserContext);
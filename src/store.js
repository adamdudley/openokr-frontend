import { createStore, applyMiddleware, compose, combineReducers } from 'redux'
import thunk from 'redux-thunk'
import objectiveReducer from './objectives/objectiveReducer'
import currentUserReducer from './authentication/currentUserReducer';

const reducer = combineReducers({
    objectives: objectiveReducer,
    user: currentUserReducer
    // key: keyReducer,
    // loginForm: loginFormReducer,
    // signUpForm: signUpReducer
})

const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(reducer, composeEnhancer(applyMiddleware(thunk)))

export default store
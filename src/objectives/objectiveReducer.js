export default (state = [], action) => {
    switch (action.type) {
        case "SET_OBJECTIVES":
            return action.objectives

        default:
            return state
    }
}
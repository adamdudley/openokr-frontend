import React, { useState } from 'react';
import { ListGroup, Container, Row, Col} from 'react-bootstrap'
import { keyResultStyle } from '../inlinestyles'
// import KeyResultForm from '../keyResults/KeyResultForm'
import axios from 'axios'
import { API_OBJECTIVES_URL } from '../api/APIUtils';
import { deleteButtonStyle, editButtonStyle } from '../inlinestyles'
import { ButtonGroup, Button, Accordion, Card, AccordionSection } from 'react-rainbow-components'
import { Link } from 'react-router-dom'
import KeyResultModalForm from '../keyResults/KeyResultModalForm';


export default function ObjectiveCard(props) {
    const [results, setResults] = useState(props.objective.key_results)

    function pushNewKR(data) {
        setResults([...results, data]);
    }
    
    const handleDeleteObjective = e => {
        e.preventDefault()
        props.removeDeletedObjective(props.objective.id)
        axios({
            method: 'delete',
            url: `${API_OBJECTIVES_URL}/${props.objective.id}`,
            headers: {
                'Content-Type': 'application/json',
                Authorization: localStorage.token
            }
        })
        .then(res => {
                console.log("objective deleted")
            })
            .catch(function (error) {
                console.log(error)
            })
        }
        
    const handleDeleteResult = (e) => {
        e.preventDefault()
        setResults(results.filter(result => result.id !== parseInt(e.target.id)))
        axios({
            method: 'delete',
            url: `${API_OBJECTIVES_URL}/${props.objective.id}/key_results/${e.target.id}`,
            headers: {
                // 'Content-Type': 'application/json',
                Authorization: localStorage.token
            }
        })
        .then(res => {
            console.log("key result deleted")
            window.alert('Key result deleted')
        })
        .catch(function (error) {
            console.log(error)
        })
    }
    
    return(
        <AccordionSection label={`Objective: ${props.objective.description}`} eventKey={props.objective.id}>
            <Row className="m-3"> 
                <Col>
                    <p>Score: {props.objective.score}</p>
                </Col>
                <Col>
                    <ButtonGroup className="rainbow-m-around_medium">
                        <Button label="Delete Objective" style={deleteButtonStyle} onClick={handleDeleteObjective} />
                        <Link to={{
                            pathname: "/objectiveEdit",
                            state: {
                                objective: props.objective
                            }
                        }}> <Button label="Edit Objective" style={editButtonStyle}/></Link>
                    </ButtonGroup>
                </Col>
            </Row>
            <Accordion eventKey={props.objective.id}>
                {results.map(kR =>
                        <Card>
                    <AccordionSection label={`Key Result: ${kR.description}`} key={kR.id} style={keyResultStyle}>

                        <Row>
                            <Col>
                                <p>Due Date: {kR.due_date}</p>
                            </Col>
                            <Col>
                                <ButtonGroup className="rainbow-m-around_medium">
                                    <Button 
                                        label="Delete Key Result" 
                                        style={deleteButtonStyle} 
                                        id={kR.id}
                                        onClick={handleDeleteResult} />
                                    <Link to={{
                                        pathname: "/keyResultEdit",
                                        state: {
                                            keyResult: kR,
                                            objectiveId: props.objective.id
                                        }
                                    }}> <Button label="Edit Key Result" style={editButtonStyle} /></Link>
                                </ButtonGroup>
                            </Col>
                        </Row>
                    </AccordionSection>
                        </Card>
                )}    
                {/* <Card className="mt-3" title="Add a new Key Result">
                        <KeyResultForm objectiveId={props.objective.id} pushNewKR={pushNewKR}/>
                </Card> */}
                <KeyResultModalForm objectiveId={props.objective.id} pushNewKR={pushNewKR}/>
            </Accordion>
        </AccordionSection>
    )
}
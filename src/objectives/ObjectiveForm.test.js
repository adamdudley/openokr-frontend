// Adapted from https://dev.to/richardigbiriki/testing-your-first-react-component-with-jest-and-enzyme-p38

import React from 'react';
import { shallow } from 'enzyme';
import ObjectiveForm from './ObjectiveForm';

describe('Objective form', () => {
    const wrapper = shallow(<ObjectiveForm />);

    it('should have a btn component', () => {
        //There should be only one button
        expect(wrapper.find('Button')).toHaveLength(1);

        //Button should be of type button
        expect(wrapper.find('Button')
            .type().defaultProps.type)
            .toEqual('button');

        //Button should have matching text
        expect(wrapper.find('Button').text()).toEqual('Add Objective');
    });

    it('should have input for description and score', () => {
        //Email and password input field should be present
        expect(wrapper.find('#description')).toHaveLength(1);
        expect(wrapper.find('#score')).toHaveLength(1);
    });
})
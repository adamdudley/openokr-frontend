import React, { useState, useEffect } from 'react'
import axios from 'axios'
import { Container } from 'react-bootstrap'
import ObjectiveCard from './ObjectiveCard'
import ObjectiveForm from './ObjectiveForm'
import { useUser } from '../hooks/UseUser'
import {Spinner, Accordion, Card} from 'react-rainbow-components'

import { API_OBJECTIVES_URL } from '../api/APIUtils'

export default function ObjectiveList() {
  const [objectives, setObjectives] = useState([])
  const [load, setLoad] = useState(false)
  const [error, setError] = useState('')
  const { user } = useUser()

  useEffect(() => {
    axios({
      method: 'get',
      url: `${API_OBJECTIVES_URL}`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: localStorage.token // this is where token goes accessed from localStor
      }
    })
      .then(res => {
        setObjectives(res.data)
        setLoad(true)
      })
      .catch(err => {
        setError(err.message)
        setLoad(true)
      })
  }, [])

  function pushNewObjective(data) {
    setObjectives([...objectives, data])
  }

  const removeDeletedObjective = objectiveId => {
    setObjectives(objectives.filter(objective => objective.id !== objectiveId))
  }

  return (
    <div>
      {
        !user.name || !load
        ?
          <Spinner size="large" variant="neutral"/>
        :
        <Container>
          <h3 className="mt-3">Welcome, {user.name}.</h3>
          <div className="rainbow-m-around_xx-large">
              <Card className="mt-3" title="Your Objectives">
              <Accordion>
                {objectives.map(objective => (
                  <ObjectiveCard key={objective.id} objective={objective} removeDeletedObjective={removeDeletedObjective} />
                  ))}
              </Accordion>
            </Card>
          </div>
          <Card className="mt-3" title="Add a new Objective">
            <ObjectiveForm pushNewObjective={pushNewObjective} />
          </Card>
        </Container>
      }
    </div>
  )
}

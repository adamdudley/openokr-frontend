import React, { useState } from 'react'
import Form from 'react-bootstrap/Form'
import Container from 'react-bootstrap/Container'
import axios from 'axios'
import {alertStyle} from '../inlinestyles'
import { Button, Input, Slider, Notification } from 'react-rainbow-components';
import { buttonStyle } from '../inlinestyles'
import { API_OBJECTIVES_URL } from '../api/APIUtils'

const ObjectiveForm = props => {
  const [description, setDescription] = useState('')
  const [score, setScore] = useState(0.6);
  const [notification, setNotification] = useState(false)


  const handleSubmit = e => {
    const objective = {
      description: description,
      score: score
    }
    e.preventDefault()
    axios({
      method: 'post',
      url: `${API_OBJECTIVES_URL}`,
      headers: {
        'Content-Type': 'application/json',
        Authorization: localStorage.token // this is where token goes accessed from localStor
      },
      data: objective
    })
      .then(res => {
        props.pushNewObjective(res.data)
      })
      .catch(function(error) {
        console.log(error)
        setNotification(true);
      })
  }

  return (
    <Container className="mb-3">
      {notification &&
        <Notification
        style={alertStyle}
          onRequestClose={() => setNotification(false)}
          title="Please check your description and score"
          description="Objective description and score between 0.1 and 1.0 are required"
        />}
      <Form className='p-2 mb-3' onSubmit={handleSubmit}>
        <Form.Group className='mb-3'>
          <Input
            id='description'
            type='text'
            value={description}
            onChange={e => setDescription(e.target.value)}
            name='description'
            placeholder='Description'
            label='Description'
            required
          />
        </Form.Group>
        <Form.Group className='mb-3'>
          <Slider
            label="Score"
            min="0.1"
            max="1.0"
            step="0.1"
            value={score}
            onChange={e => setScore(e.target.value)}
          />
        </Form.Group>
        <Form.Group>
          <Button
            type="submit"
            label="Add Objective"
            style={buttonStyle}
          />
        </Form.Group>
      </Form>
    </Container>
  )
}

export default ObjectiveForm

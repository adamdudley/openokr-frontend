// synchronous actions
export const setObjectives = objectives => {
    return {
        type: "SET_OBJECTIVES",
        objectives
    }
}

// async actions
export const addObjective = (description, score, label, type, userId, history) => {
    return (dispatch) => {
        const objectiveData = {
            objective: {
                description: description,
                score: score,
                label: label,
                type: type,
                userId: userId
            }
        }
        fetch(`localhost:4000/api/v1/objectives`, {
            credentials: 'include',
            headers: {
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            },
            method: 'POST',
            body: JSON.stringify(objectiveData)
        })
            .then(response => response.json())
            .then(objectives => {
                dispatch(setObjectives(objectives.data))
            })
        history.push('/objectives')
    }
}
import React, { useState, useEffect } from 'react'
import Form from 'react-bootstrap/Form'
import { Container, Row, Col } from 'react-bootstrap'
import axios from 'axios'
import { alertStyle } from '../inlinestyles';
import { useHistory, useLocation, Link } from 'react-router-dom';
import { Button, Input, Slider, Notification, Card } from 'react-rainbow-components';
import { buttonStyle } from '../inlinestyles'
import { deleteButtonStyle } from '../inlinestyles'

import { API_OBJECTIVES_URL } from '../api/APIUtils'

const ObjectiveEditForm = props => {

    const [description, setDescription] = useState("")
    const [score, setScore] = useState(0.1);
    const [id, setId] = useState()
    const [notification, setNotification] = useState(false)
    let history = useHistory();
    let location = useLocation();

    useEffect(() => {
        const { objective } = location.state;
        setDescription(objective.description);
        setScore(objective.score);
        setId(objective.id);
    }, [location.state])

    const handleSubmit = e => {
        const objective = {
            description: description,
            score: score
        }
        e.preventDefault()
        axios({
            method: 'put',
            url: `${API_OBJECTIVES_URL}/${id}`,
            headers: {
                'Content-Type': 'application/json',
                Authorization: localStorage.token // this is where token goes accessed from localStor
            },
            data: objective
        })
            .then(res => {
                if(res.status === 204) {
                    history.push("/objectiveList")

                } else {
                    setNotification(true);
                }
            })

            .catch(function (error) {
                console.log(error)
        
            })
    }

    return (
        <Container>
            <Card title="Edit Objective" className="mt-3">
            {notification 
                &&
                <Notification
                style={alertStyle}
                onRequestClose={() => setNotification(false)}
                title="Please check your description and score"
                description="Objective description and score between 0.1 and 1.0 are required"
                />
            }
            <Form className='m-3' onSubmit={handleSubmit}>
                <Form.Group className='mb-3'>
                    {/* <Form.Label>Description</Form.Label> */}
                    <Input
                        id='description'
                        type='text'
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                        name='description'
                        placeholder='Description'
                        label='Description'
                        required
                        />
                </Form.Group>
                <Form.Group className='mb-3'>
                    <Slider
                        label="Score"
                        min="0.1"
                        max="1.0"
                        step="0.1"
                        value={score}
                        onChange={e => setScore(e.target.value)}
                        />
                </Form.Group>
                <Row className="justify-content-md-center">
                    <Col md="auto">
                        <Form.Group>
                            <Button
                                type="submit"
                                label="Save Changes"
                                style={buttonStyle}
                            />
                        </Form.Group>
                    </Col>
                    <Col md="auto">
                        <Link to={{
                            pathname: "/objectiveList"
                        }}> <Button label="Cancel" style={deleteButtonStyle} /></Link>
                    </Col>
                </Row>
            </Form>
            </Card>
        </Container>
    )
}

export default ObjectiveEditForm
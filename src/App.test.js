import React from 'react';
import { render } from '@testing-library/react';
import App from './App';
import { shallow } from 'enzyme';

const wrapper = shallow(<App />)
test('should render a header', () => {
  expect(wrapper.find("h1").text()).toEqual('Welcome to Open OKR')
});

import React from 'react'
import { Link } from 'react-router-dom'
import { Container, ListGroup, Item } from 'react-bootstrap'

const About = () => {
  return (
    <Container className='mainContainer'>
      <h1 className='mainHeadingTwo'>From intention to achievement</h1>
      <hr />
      <p>
        OpenOKR gives you the clarity of focus you need to achieve all the
        things that are important to you.
      </p>
      <p>
        Objectives & Key Results (OKRs) are a neat productivity model used by
        individuals and the teams in numerous big names in tech like GitLab,
        Google, Twitter, Spotify, Netflix, Amazon, Facebook, Intel (ground-zero
        for OKRs!) just to name a few.
      </p>
      <p>You can learn more about OKRs at the following three links:</p>
      <ListGroup>
        <ListGroup.Item>
          <Link
            to='https://assets.ctfassets.net/cn6v7tcah9c0/4snZXJ821G6KYUoc08masK/58ffcbc7c607d7c6056c7da507727135/Google_OKR_Playbook_V1JS.pdf'
            target='_blank'
          >
            Google OKR Playbook
          </Link>
        </ListGroup.Item>
        <ListGroup.Item>
          <Link to='https://www.whatmatters.com/' target='_blank'>
            WhatMatters.com
          </Link>
        </ListGroup.Item>
        <ListGroup.Item>
          <Link
            to='https://blog.weekdone.com/what-companies-use-okrs/'
            target='_blank'
          >
            Companies that use OKRs
          </Link>
        </ListGroup.Item>
      </ListGroup>
    </Container>
  )
}

export default About

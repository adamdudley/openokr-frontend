import React, { useState } from 'react';
import { Form, Container, Row, Col } from 'react-bootstrap'
import axios from 'axios'
import { Modal, Button, Input, DatePicker, Textarea} from 'react-rainbow-components';
import { API_OBJECTIVES_URL } from '../api/APIUtils'
import { cancelButtonStyle, addButtonStyle } from '../inlinestyles'

const styles = {
    input: {
        marginTop: 24,
    },
    inputColumn: {
        width: '48%',
        marginTop: 24,
    }
};

function KeyResultModalForm(props) {
    const [description, setDescription] = useState('')
    const [date, setDate] = useState(new Date());
    const [isOpen, setOpenStatus] = useState(false);
    let dueDate
    
    const handleSubmit = e => {
        e.preventDefault()
        dueDate = date.toLocaleDateString()
        const keyResult = {
            description: description,
            due_date: dueDate
        }
        axios({
            method: 'post',
            url: `${API_OBJECTIVES_URL}/${props.objectiveId}/key_results`,
            headers: {
                'Content-Type': 'application/json',
                Authorization: localStorage.token
            },
            data: keyResult
        })
            .then(res => {
                props.pushNewKR(res.data)
                setDescription('');
                setDate(new Date());
                setOpenStatus(false);
            })
            .catch(function (error) {
                console.log(error)
            })
    }
    
    return (
        <Container>
            <Button
                id="button-12"
                style={addButtonStyle}
                label="Add a Key Result"
                onClick={() => setOpenStatus(true)}
                />
            <Modal
                id="modal-12"
                title="Add your key result"
                isOpen={isOpen}
                onRequestClose={() => setOpenStatus(false)}
                >
                <Form onSubmit={handleSubmit}>
                    <Form.Group>
                        <Input
                            id='keyResultDescription'
                            className="rainbow-m-bottom_medium"
                            style={styles.input}
                            component={Textarea}
                            name="description"
                            label="Description"
                            placeholder="Add description"
                            value={description}
                            onChange={e => setDescription(e.target.value)}
                            required
                            />
                    </Form.Group>
                    <Form.Group>
                        <div className="rainbow-flex rainbow-justify_spread">
                            <DatePicker
                                required
                                value={date}
                                label="Due Date"
                                onChange={e => setDate(e)}
                                />
                        </div>
                    </Form.Group>
                    <Form.Group>
                        <div className="rainbow-flex rainbow-justify_end">
                            <Row>
                                <Col>
                                    <Button
                                        className="rainbow-m-right_large"
                                        label="Cancel"
                                        style={cancelButtonStyle}
                                        onClick={() => setOpenStatus(false)}
                                        />
                                </Col>
                                <Col>
                                    <Button
                                        label="Save"
                                        style={addButtonStyle}
                                        type="submit"
                                    />
                                </Col>
                            </Row>
                        </div>
                    </Form.Group>
                </Form>
            </Modal>
        </Container>
    );
};

export default KeyResultModalForm;
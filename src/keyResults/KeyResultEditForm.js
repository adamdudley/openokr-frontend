import React, { useState, useEffect } from 'react'
import Form from 'react-bootstrap/Form'
import { Container, Row, Col} from 'react-bootstrap'
import axios from 'axios'
import { alertStyle } from '../inlinestyles';
import { useHistory, useLocation, Link } from 'react-router-dom';
import { Button, Input, DatePicker, Notification, Card } from 'react-rainbow-components';
import { buttonStyle, deleteButtonStyle } from '../inlinestyles'

import { API_OBJECTIVES_URL } from '../api/APIUtils'

const KeyResultEditForm = props => {

    const [description, setDescription] = useState("")
    const [date, setDate] = useState(new Date());
    const [objectiveId, setObjectiveId] = useState()
    const [keyResultId, setKeyResultId] = useState()
    const [notification, setNotification] = useState(false)
    let history = useHistory();
    let location = useLocation();
    let dueDate;

    useEffect(() => {
        const { keyResult } = location.state;
        const { objectiveId } = location.state
        setDescription(keyResult.description);
        setDate(keyResult.date);
        setObjectiveId(objectiveId);
        setKeyResultId(keyResult.id);
    }, [location.state])

    const handleSubmit = e => {
        e.preventDefault()
        dueDate = date.toLocaleDateString()
        const keyResult = {
            description: description,
            due_date: dueDate
        }
        axios({
            method: 'put',
            url: `${API_OBJECTIVES_URL}/${objectiveId}/key_results/${keyResultId}`,
            headers: {
                'Content-Type': 'application/json',
                Authorization: localStorage.token
            },
            data: keyResult
        })
            .then(res => {
                if (res.status === 204) {
                    history.push("/objectiveList")
                    setDescription('');
                    setDate(new Date());
                } else {
                    setNotification(true);
                }
            })
            .catch(function (error) {
                console.log(error)
            })
    }

    return (
        <Container>
            <Card title="Edit Key Result" className="mt-3">
            {notification 
                &&
                <Notification
                    style={alertStyle}
                    onRequestClose={() => setNotification(false)}
                    title="Please check your description and due date"
                    description="Description and due date required"
                />
            }
            <Form onSubmit={handleSubmit} className='m-3'>
                <Form.Group className='mb-3'>
                    <Input
                        id='keyResultDescription'
                        type='text'
                        value={description}
                        onChange={e => setDescription(e.target.value)}
                        name='description'
                        placeholder='Description'
                        label='Add a description'
                        required
                    />
                </Form.Group>
                <Form.Group>
                    <DatePicker
                        required
                        value={date}
                        label="Due Date"
                        onChange={e => setDate(e)}
                    />
                </Form.Group>
                <Row className="justify-content-md-center">
                    <Col md="auto">
                        <Form.Group>
                            <Button
                                type="submit"
                                label="Save Changes"
                                style={buttonStyle}
                                />
                        </Form.Group>
                    </Col>
                    <Col md="auto">
                        <Link to={{
                            pathname: "/objectiveList"
                        }}> <Button label="Cancel" style={deleteButtonStyle} /></Link>
                    </Col>
                </Row>
            </Form>
            </Card>
        </Container>
    )
}

export default KeyResultEditForm
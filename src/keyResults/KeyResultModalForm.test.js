import React from 'react';
import { shallow } from 'enzyme';
import KeyResultModalForm from './KeyResultForm';

describe('Key Result form', () => {
    const wrapper = shallow(<KeyResultModalForm />);

    it ('should have a btn component', () => {
        expect(wrapper.find('Button')).toHaveLength(1);

        expect(wrapper.find('Button')
            .type().defaultProps.type)
            .toEqual('button');

        expect(wrapper.find('Button').text()).toEqual('Add Key Result');
    });

    it ('should have input for description and due date', () => {
        expect(wrapper.find('#keyResultDescription')).toHaveLength(1);
        expect(wrapper.find('#due_date')).toHaveLength(1);
    });
})